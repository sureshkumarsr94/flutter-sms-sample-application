import 'package:flutter/material.dart';
import 'package:sms/sms.dart';

void main() => runApp(new SmsApp());

class SmsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'SMS Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: new SmsPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class SmsPage extends StatefulWidget {
  SmsPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  State<StatefulWidget> createState() => new _SmsAppPageState();
}

class _SmsAppPageState extends State<SmsPage> {
  final phoneController = TextEditingController();
  final messageController = TextEditingController();

  @override
  void dispose() {
    phoneController.dispose();
    messageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: const Text('SMS Demo Application'),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //SizedBox(height: 20.0),
              Container(
                padding: EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
                child: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                        labelText: "Phone Number",
                        hintText: "+919876543210",
                        labelStyle: TextStyle(
                            color: Colors.grey, fontWeight: FontWeight.bold),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                      ),
                      controller: phoneController,
                      keyboardType: TextInputType.phone,
                      maxLength: 15,
                    ),
                    SizedBox(height: 20),
                    ButtonTheme(
                      minWidth: double.maxFinite,
                      child: RaisedButton(
                        onPressed: () => readSms(),
                        color: Colors.blue,
                        child: Text(
                          'Read SMS',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(height: 50),
                    TextField(
                      decoration: InputDecoration(
                        labelText: "Message",
                        labelStyle: TextStyle(
                            color: Colors.grey, fontWeight: FontWeight.bold),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                      ),
                      controller: messageController,
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      maxLength: 150,
                    ),
                    SizedBox(height: 20),
                    ButtonTheme(
                      minWidth: double.maxFinite,
                      child: RaisedButton(
                        onPressed: () => sendSms(),
                        color: Colors.blue,
                        child: Text(
                          'Send SMS',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  Future readSms() async {
    SmsQuery query = new SmsQuery();
    //address: '9742024068',9790970371
    List<SmsMessage> sms = await query.querySms(
        address: phoneController.text, kinds: [SmsQueryKind.Inbox]);

    if (sms.length > 0) {
      showAlert(sms[0].body);
    } else {
      showAlert("No Message Found");
    }
  }

  sendSms() {
    SmsSender sender = new SmsSender();
    String address = phoneController.text;
    SmsMessage message = new SmsMessage(address, messageController.text);
    message.onStateChanged.listen((state) {
      String response;
      if (state == SmsMessageState.Sent) {
        showAlert("Sms is sent!");
      } else if (state == SmsMessageState.Delivered) {
        showAlert("Sms is delivered!");
      } else if (state == SmsMessageState.Fail) {
        showAlert("Sms is failed!");
      }
    });
    sender.sendSms(message);
  }

  showAlert(message) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(content: Text(message));
      },
    );
  }
}
